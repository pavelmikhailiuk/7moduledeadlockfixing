package deadlock.main;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

import deadlock.thread.NumberAdder;
import deadlock.thread.NumberSquaresSqareRootCalculator;
import deadlock.thread.NumberWriter;

public class Runner {
	public static void main(String[] args) {

		List<Integer> list = new ArrayList<>();

		ReadWriteLock lock = new ReentrantReadWriteLock();

		new Thread(new NumberWriter(lock, list)).start();

		new Thread(new NumberAdder(lock, list)).start();

		new Thread(new NumberSquaresSqareRootCalculator(lock, list)).start();
	}
}
