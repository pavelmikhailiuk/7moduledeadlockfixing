package deadlock.thread;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import org.apache.log4j.Logger;

public class NumberAdder implements Runnable {

	private static final Logger LOG = Logger.getLogger(NumberAdder.class);
	private ReadWriteLock lock;

	private List<Integer> list;

	public NumberAdder(ReadWriteLock lock, List<Integer> list) {
		this.lock = lock;
		this.list = list;
	}

	@Override
	public void run() {
		while (true) {
			int sum = 0;
			try {
				lock.readLock().lock();
				for (int i = 0; i < list.size(); i++) {
					sum += list.get(i);
				}
			} finally {
				lock.readLock().unlock();
			}
			LOG.info("Sum: " + sum);
		}
	}
}
