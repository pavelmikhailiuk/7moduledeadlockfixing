package deadlock.thread;

import java.util.List;
import java.util.concurrent.locks.ReadWriteLock;

import org.apache.log4j.Logger;

public class NumberSquaresSqareRootCalculator implements Runnable {

	private static final Logger LOG = Logger.getLogger(NumberSquaresSqareRootCalculator.class);

	private ReadWriteLock lock;

	private List<Integer> list;

	public NumberSquaresSqareRootCalculator(ReadWriteLock lock, List<Integer> list) {
		this.lock = lock;
		this.list = list;
	}

	@Override
	public void run() {
		while (true) {
			double squaresSum = 0;
			try {
				lock.readLock().lock();
				for (int i = 0; i < list.size(); i++) {
					squaresSum += Math.pow(list.get(i), 2);
				}
			} finally {
				lock.readLock().unlock();
			}
			LOG.info("Square root of sum of squares: " + Math.sqrt(squaresSum));
		}
	}
}
