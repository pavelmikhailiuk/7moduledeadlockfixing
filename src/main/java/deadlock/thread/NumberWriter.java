package deadlock.thread;

import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReadWriteLock;

public class NumberWriter implements Runnable {

	private static final int LIST_SIZE = 10;

	private ReadWriteLock lock;

	private List<Integer> list;

	public NumberWriter(ReadWriteLock lock, List<Integer> list) {
		this.lock = lock;
		this.list = list;
	}

	@Override
	public void run() {
		Random random = new Random();
		try {
			lock.writeLock().lock();
			for (int i = 0; i < LIST_SIZE; i++) {
				list.add(i, new Integer(random.nextInt(100)));
			}
		} finally {
			lock.writeLock().unlock();
		}
		while (true) {
			try {
				lock.writeLock().lock();
				list.set(random.nextInt(LIST_SIZE), new Integer(random.nextInt(100)));
			} finally {
				lock.writeLock().unlock();
			}
		}
	}
}
